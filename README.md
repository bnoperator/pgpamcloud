#pgMulticore

## Instal

```
devtools::install_bitbucket("bnoperator/pgpamcloud")
```

# Publish a package on pamagene R repository

```
bntools::deployGitPackage('https://bitbucket.org/bnoperator/pgpamcloud.git', '1.4')
```
# tag
```
git tag 1.4 -m "1.4" && git push --tags
